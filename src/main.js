import * as Turbo from "@hotwired/turbo"
import Alpine from 'alpinejs'

import collapse from '@alpinejs/collapse'

import usePopper from "./usePopper";

document.addEventListener('alpine:init', () => {
  Alpine.data("usePopper", usePopper);
})

import breakpoints from "./breakpoints";

Alpine.store("breakpoints", breakpoints);

window.Alpine = Alpine

Alpine.plugin(collapse)

Alpine.start()

// 目前 TailwindCSS 是通过 JS 在浏览器端动态生成 CSS，会导致页面短暂闪烁, 所以需要等待 TailwindCSS 加载完成后再显示页面
document.addEventListener("DOMContentLoaded", function(){
  var now = new Date()
  var monitor = setInterval(() => {
    if(new Date() - now > 3000) { clearInterval(monitor); return }
    document.querySelectorAll('head > style').forEach(style => {
      if(style.innerHTML.includes('/* ! tailwindcss v')) {
        console.log('tailwindcss loaded in ' + (new Date() - now) + 'ms')
        setTimeout(() => {
          document.getElementsByTagName('html')[0].classList.remove('tailwindcss-loading');
          document.querySelector('.tailwindcss-loader').remove()
        }, 100);
        clearInterval(monitor)
      }
    })
  }, 50);
})


